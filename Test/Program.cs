﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            int len = StringLength("Aboba");
            Console.WriteLine($"Aboba - {len}");

            len = StringLength("Hello, world!");
            Console.WriteLine($"Hello, world! - {len}");

            len = StringLength("");
            Console.WriteLine($" - {len}");
        }

        static int StringLength(string value)
        {
            int i = 0;
            foreach (char c in value)
            {
                i++;
            }
            return i;
        }
    }
}
