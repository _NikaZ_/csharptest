﻿using System;

namespace Test2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] {10, -20, 15, 23, 90, 34, -12, -86, 1, 33, 9, 12, 5};
            Console.WriteLine($"Array: {string.Join(", ", array)}");
            ReverseArray(ref array);
            Console.WriteLine($"Reversed array: {string.Join(", ", array)}");
        }


        static void ReverseArray(ref int[] array)
        {
            for (int i = 0; i < array.Length/2; i++)
            {
                int buff = array[i];
                array[i] = array[array.Length - i - 1];
                array[array.Length - i - 1] = buff;
            }
        }
    }
}
