﻿using System;

namespace Test3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Password: ");
            bool isValidate = ValidatePassword(Console.ReadLine());
            Console.WriteLine(isValidate ? "Correct" : "Wrong");
        }

        static bool ValidatePassword(string password)
        {
            const string SPEC_SYMBS = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            bool hasDigit = false;
            bool hasSpecSymbol = false;
            foreach (char c in password)
            {
                if(c == ' ')
                {
                    return false;
                }
                if(!hasDigit && char.IsDigit(c))
                {
                    hasDigit = true;
                }
                if(!hasSpecSymbol && SPEC_SYMBS.Contains(c))
                {
                    hasSpecSymbol = true;
                }
                if(hasDigit && hasSpecSymbol)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
