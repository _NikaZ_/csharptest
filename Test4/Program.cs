﻿using System;

namespace Test4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1 };

            CountOnesAfterZeros(array);
        }
        
        static void CountOnesAfterZeros(int[] array)
        {
            int onesCounter = 0;
            Console.WriteLine($"Index  Count");
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 1)
                {
                    onesCounter++;
                }
                else if(array[i] == 0)
                {
                    Console.WriteLine($"{i,-7}{onesCounter}");
                    onesCounter = 0;
                }
            }
        }
    }
}
